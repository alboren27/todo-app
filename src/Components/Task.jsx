import React from "react";

import "./Task.css";

const Task = props => (
  <div className="box-task">
    <button
      className={props.status}
      onClick={() => props.changeStatus(props.id)}
    />
    <h1 className="text-task">{props.text}</h1>
  </div>
);

export default Task;
