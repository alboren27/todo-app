import React from "react";
import "./TaskInput.css";

const TaskInput = props => (
  <div className="box-task-input">
    <input
      type="text"
      className="task-input"
      placeholder="Make a List!"
      onKeyUp={event => props.addTask(event)}
    />
  </div>
);

export default TaskInput;
