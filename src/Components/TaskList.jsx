import React, { Component } from "react";

import Task from "./Task";
import TaskFooter from "./TaskFooter";

import "./TaskList.css";

class TaskList extends Component {
  constructor() {
    super();
    this.state = {
      tasksToShow: [],
      filterSelection: null
    };
    this.handleSelectFilter = this.handleSelectFilter.bind(this);
  }

  handleSelectFilter(event) {
    console.log(event.target.value);
    this.setState({ filterSelection: event.target.value });
  }

  render() {
    let taskFooter = "";
    if (this.props.tasks) {
      taskFooter = (
        <TaskFooter
          activeAmount={
            this.props.tasks.filter(e => e.status === "active").length
          }
          handleSelectFilter={this.handleSelectFilter}
        />
      );
    } else {
      taskFooter = "";
    }

    return (
      <div className="box-task-list">
        {this.props.tasks.map(e => (
          <Task
            key={e.id}
            id={e.id}
            text={e.text}
            status={e.status}
            changeStatus={this.props.changeStatus}
          />
        ))}
        {taskFooter}
      </div>
    );
  }
}

export default TaskList;
