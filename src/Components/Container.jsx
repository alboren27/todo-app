import React, { Component } from "react";

import TaskInput from "./TaskInput";
import TaskList from "./TaskList";

import "./Container.css";

class Container extends Component {
  constructor() {
    super();
    this.state = {
      tasks: []
    };
    this.addTask = this.addTask.bind(this);
    this.changeStatus = this.changeStatus.bind(this);
  }

  componentDidMount() {
    const tasks = JSON.parse(localStorage.getItem("tasks"));
    if (tasks) {
      this.setState({ tasks });
    }
  }

  addTask(event) {
    if (event.keyCode === 13) {
      const tasks = [...this.state.tasks];
      tasks.push({
        id: tasks.length === 0 ? 0 : tasks[tasks.length - 1].id + 1,
        text: event.target.value,
        status: "active"
      });
      this.setState({ tasks });
      localStorage.setItem("tasks", JSON.stringify(tasks));
    }
  }

  changeStatus(id) {
    const tasks = [...this.state.tasks];
    tasks.map(e => {
      if (e.id === id) {
        if (e.status === "active") {
          e.status = "done";
        } else {
          e.status = "active";
        }
      }
    });
    this.setState({ tasks });
    localStorage.setItem("tasks", JSON.stringify(tasks));
  }

  render() {
    return (
      <div className="box-container">
        <TaskInput addTask={this.addTask} />
        <TaskList tasks={this.state.tasks} changeStatus={this.changeStatus} />
      </div>
    );
  }
}

export default Container;
