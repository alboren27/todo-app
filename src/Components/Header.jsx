import React from "react";

import "./Header.css";

const Header = () => (
  <header className="box-header">
    <h2 className="top-header">TODOs</h2>
  </header>
);

export default Header;
