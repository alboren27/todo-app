import React from "react";

import "./TaskFooter.css";

const TaskFooter = props => (
  <div className="box-task-footer">
    <p>{props.activeAmount} items left</p>
    <div>
      <input
        type="radio"
        id="all"
        name="select"
        value="all"
        defaultChecked
        onChange={event => props.handleSelectFilter(event)}
      />
      <label>All</label>
      <input
        type="radio"
        id="active"
        name="select"
        value="active"
        onChange={event => props.handleSelectFilter(event)}
      />
      <label>Active</label>
      <input
        type="radio"
        id="completed"
        name="select"
        value="done"
        onChange={event => props.handleSelectFilter(event)}
      />
      <label>Completed</label>
    </div>
  </div>
);

export default TaskFooter;
